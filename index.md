<div class="topnav">
    <a href="/" class="active">
        <h1>Projects</h1>
    </a>
    <a href="experience.html">
        <h1>Experience</h1>
    </a>
    <a href="skills.html">
        <h1>Skills</h1>
    </a>
</div>

# Adham Zahran
adhamzahranfms@gmail.com

A software engineer. A jack of all trades, master of none.

In this website, I will tell you all about my not-so-amazing skills.

## Professional Projects

### Salem
#### Jan 2019 - Apr 2023

<img src="assets/salem.png">

Associated with RDI

This project required extensive coordination, technical expertise, and dedication to ensure its success.

In a project led by Amr Elgamal, I had the honor of playing both the roles of a project manager and an engineer. The project aimed to teach kids the Arabic letters, their forms, and pronunciations with different diacritics (Fatha, Damma, and Kasra), in addition to teaching the recitation of Al-Fateha.  

As a project manager, I led the research and development of the backend while coordinating the entire project. My engineering role included C++ development, Python (Flask), and cloud engineering on Google Cloud Platform.  

Responsibilities included:  

- Overseeing data annotation for machine learning  
- Ensuring the availability of necessary tools for data annotators  
- Managing the machine learning team and providing resources like annotated data and GPUs  
- Developing the engine in C++  
- Developing the backend for machine learning endpoints in Python  
- Engineering cloud infrastructure using Google Cloud Platform services  
- Benchmarking and optimizing server performance for scalability  
- Coordinating between testers and developers  
- Porting the machine learning platform to work natively on Android and iOS without internet

<a href="https://play.google.com/store/apps/details?id=com.tafseer.salem">
<img src="assets/google_play.png">
</a>

<a href="https://apps.apple.com/us/app/%D8%B3%D8%A7%D9%84%D9%85-salem/id1623387105">
<img src="assets/app_store.png">
</a>

#### Skills:
Databases . MySQL . Technical Leadership . Project Management . Hands-on Technical Leadership . C++

### Sotoor, Kateb, Natiq and Tashkeel
#### Apr 2017 - Jun 2019

Associated with RDI

I worked as a systems engineer developing the C++ engines behind the AI suite of RDI: Sotoor, Kateb, Natiq and Tashkeel. OCR, ASR, TTS and Automatic diacratization of Arabic respectively.  

The engines do feature extraction, feed forward, language model rescoring. They also interoperate with other languages to be usable by backend servers like java, python and C#  

My responsibilities included:  

- Multi-threading, and writing thread-safe code  
- Interprocess communication  
- Server programming, network protocol implementation  
- Interoperating C/C++ with other languages: Java, Python, C#  
- Cross-compiling and porting to Android and IOS  
- Different memory management techniques  
- Build system and dependency management  
- Unit-testing, Integration testing, CI and CD  
- Parse and create file formats  
- Designing system architectures  
- Planning and team management  

#### Sotoor
<a href="https://sotoor.ai">
    <img src="assets/sotoor.png">
</a>

#### Kateb
<a href="https://kateb.ai">
    <img src="assets/kateb.png">
</a>

#### Natiq
<a href="https://rdi-natiq.com">
    <img src="assets/natiq.png">
</a>

#### Tashkeel
<a href="https://rdi-tashkeel.com">
    <img src="assets/tashkeel.png">
</a>

#### Skills:
C++ . Multithreading . Inter-process Communication . Interop . Memory Management . Server Programming . Network Programming . Cross-Compiling . CMake . Unit Testing . Continuous Integration (CI) . Continuous Integration and Continuous Delivery (CI/CD) . Parsing . Architectural Design . Project Management . Team Leadership . Technical Leadership

### Hand-drawn Animation Autocomplete
#### Dec 2016 - Apr 2017

<span>
<img src="assets/animation_autocomplete1.gif">
<img src="assets/animation_autocomplete2.gif">
</span>

Associated with Ain Shams University

Our work is based on a paper by Jun Xing et. al. Called Autocomplete Painting Repetitions Which detects the patterns in the artists strokes and tries to predict the upcoming strokes accordingly.
Here's a video by the original authors of a working prototype that shows the capabilities of the Autocomplete Painting Repetitions.

There is another paper by the same author called Autocomplete Hand-drawn Animations Which takes the pattern detection to be across frames.  
Here's a video for the Autocomplete Hand-drawn Animations.

[https://github.com/opentoonz/opentoonz/pull/1350](https://github.com/opentoonz/opentoonz/pull/1350)

#### Skills:
Computer Graphics . C++ . CMake

## Side Projects (The cool projects)

### Ugly UI 
#### Apr 2024 - Present

<img src="assets/ugly_ui.jpeg">

I attempted to re-create Qt's QML from scratch using C

[https://gitlab.com/lordadamson/ugly_ui](https://gitlab.com/lordadamson/ugly_ui)

#### Skills:

Parsing . QML . Lexical Analysis . C (Programming Language)

### Gollash
#### Aug 2023 - Dec 2023

<img src="assets/gollash.png">

A visual shell language, and a pipe multiplexer. The idea is to visually create nodes and connect them together. So you connect stdin, stdout and stderr visually. Unlike text you are not limited to pipe to only one process.

[https://gitlab.com/lordadamson/golash](https://gitlab.com/lordadamson/golash)

#### Skills:

Inter-process Communication . Qt . Qt Creator . IPC . Unix Pipeline . QML . C++

### Bile Manager
#### Jun 2021 - Sep 2023

<img src="assets/bile_manager.gif">

A file manager. The UI is programmed from scratch using HTML5 canvas. The backend is a go server.

[https://gitlab.com/lordadamson/bilemanager](https://gitlab.com/lordadamson/bilemanager)

#### Skills:

Cascading Style Sheets (CSS) . Web Servers . JavaScript . HTML5 Canvas . Graphical User Interface (GUI) . HTML . Go (Programming Language)

### Habit Tracker
#### Jun 2021 - Aug 2023

<img src="assets/habit_tracker.jpeg" width="500">

I created a habit tracker inspired by github's contribution chart to track the good habits I want to gain, and the bad habits I want to lose :D  

[https://gitlab.com/lordadamson/habit_tracker](https://gitlab.com/lordadamson/habit_tracker)

#### Skills:
SQLite . Databases . Cascading Style Sheets (CSS) . JavaScript . Scalable Vector Graphics (SVG) . HTML . PHP

### Bath Tracer
#### Mar 2023 - Apr 2023

<img src="assets/bath_tracer.jpeg">

A simple path tracer that I made following the ray tracer in one weekend book: [https://raytracing.github.io/books/RayTracingInOneWeekend.html](https://raytracing.github.io/books/RayTracingInOneWeekend.html)

[https://gitlab.com/lordadamson/bath_tracer](https://gitlab.com/lordadamson/bath_tracer)

#### Skills:
Computer Graphics . Ray Tracing . C (Programming Language)

### Sneaky Malloc
#### Apr 2020 - Jul 2021

<img src="assets/sneaky_malloc.gif">

We intercept calls to malloc, write the data to disk, so that it can later be visualized. This is a seed for a memory profiler. It produces visuals that are very cool.  

[https://gitlab.com/lordadamson/sneakymalloc](https://gitlab.com/lordadamson/sneakymalloc)

#### Skills:
Profiler . Ffmpeg . C (Programming Language) . Visualization

### Terrain Generation
#### May 2021 - May 2021

<iframe width="560" height="315" src="https://www.youtube.com/embed/2DES7V11nvg?si=FB65F4hRHGio5lLl" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Terrain generation using open simplex in Unity3D  

[https://gitlab.com/lordadamson/unity-terrian-generation](https://gitlab.com/lordadamson/unity-terrian-generation)

#### Skills:
C# . Simplex Algorithm . 3D Graphics

### Journal QML
#### Jul 2020 - Jul 2020

<img src="assets/journal_qml.gif">

A note taking application inspired by OneNote. Implemented using Qt C++ and QML.  

[https://gitlab.com/lordadamson/journal_qml](https://gitlab.com/lordadamson/journal_qml)

#### Skills:
Qt . Qt Creator . QML . C++

### The Internet Canvas
#### Mar 2020 - Apr 2020

<img src="assets/internet_canvas.jpeg">

A canvas on the browser where people can go type and draw stuff, and it would synchronize in real time.  

Server is written in C, implements web sockets.  
Client is javascript on the browser, uses the HTML5 canvas.  

[https://gitlab.com/lordadamson/the_internet_canvas](https://gitlab.com/lordadamson/the_internet_canvas)

#### Skills:
Realtime Programming . WebSocket . JavaScript . HTML . HTML5 Canvas . C (Programming Language)

### vworld
#### Jan 2020 - Jan 2020

<iframe width="560" height="315" src="https://www.youtube.com/embed/1L3-nfTXr0E?si=V6zIz59ChBr7B3jd" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

It's a chat client and server using Godot RPC  

Does some funny animations like /sad /happy and others. They're inspired by "1 hour 1 life" game  

[https://gitlab.com/lordadamson/vworld](https://gitlab.com/lordadamson/vworld)

#### Skills:
Remote Procedure Call (RPC) . Godot . GDScript

### Digit Recognition
#### Sep 2018 - Sep 2018

An implementation to a very simple neural network in C++ for learning purposes.  

Recognizes images of numbers by training on the MNIST dataset.  

[https://gitlab.com/lordadamson/digit_recognition](https://gitlab.com/lordadamson/digit_recognition)

#### Skills:
Neural Networks . C++

### Journal
#### Mar 2017 - Sep 2018

<img src="assets/journal.png" width="1000">

A note taking application that is heavily inspired by OneNote.

[https://gitlab.com/lordadamson/Journal](https://gitlab.com/lordadamson/Journal)

#### Skills:
Qt . Qt Creator . C++ . Object-Oriented Programming (OOP)

### std::tuple and tie() implementation
#### Jul 2018 - Sep 2018

Implementation of std::tuple and tie() for learning purposes.  

[https://gitlab.com/lordadamson/implementations_of_cpp_stuff](https://gitlab.com/lordadamson/implementations_of_cpp_stuff)

#### Skills:
Template Metaprogramming . C++17 . C++14 . C++ . C++11

### Notix
#### Mar 2018 - Mar 2018

<img src="assets/notix.jpeg" width="1000">

A simple sticky notes app built with Qt Quick  

[https://gitlab.com/lordadamson/Notix](https://gitlab.com/lordadamson/Notix)

#### Skills:
Qt . QML . C++ . Qt Creator

### QuickCD
#### Oct 2017 - Oct 2017

Bookmark locations that you visit frequently in your terminal!  

[https://gitlab.com/lordadamson/QuickCD](https://gitlab.com/lordadamson/QuickCD)

#### Skills:
Bash

### php framework
#### Feb 2017 - Mar 2017

<img src="assets/phpblog.jpeg">

I Created an php MVC framework from scratch inspired by Laravel  

[https://gitlab.com/lordadamson/phpblog](https://gitlab.com/lordadamson/phpblog)

#### Skills:
Model-View-Controller (MVC) . Databases . composer . MySQL . MariaDB . PHP

### Al Ma7faza - Allowance Calculator
#### Feb 2017 - Feb 2017

<img src="assets/ma7faza.jpeg">

Associated with Ain Shams University

An Android Application that helps you manage your money.  
The idea is quite simple. You tell it how much money you have and for how long is that money supposed to keep you alive. It divides the money over the number of days to give you a daily allowance. Every time you spend money, you let it know and every time you get money. It gonna recalculate your daily allowance everyday.  

[https://github.com/Open-Source-Community/AllowanceCalculator](https://github.com/Open-Source-Community/AllowanceCalculator)

#### Skills:
Java . Android

### groupsNmessages
#### Feb 2017 - Feb 2017

Communicate with your team in Facebook-like groups and messages without being distracted by Facebook.  

Created using Laravel.  

[https://gitlab.com/lordadamson/groupsNmessages](https://gitlab.com/lordadamson/groupsNmessages)

#### Skills:
Model-View-Controller (MVC) . Databases . PHP . Laravel

### Heat Flow Simulator
#### Jan 2017 - Jan 2017

<iframe width="560" height="315" src="https://www.youtube.com/embed/xj4_Mxq3TpM?si=xLMzoFA5VzZDKCYE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Associated with Ain Shams University

This is a simple heat flow simulator that I had done back in university.  

Done using Qt widgets.  

[https://gitlab.com/lordadamson/visualization-heat_flow](https://gitlab.com/lordadamson/visualization-heat_flow)

#### Skills:
Qt . Qt Creator . C++ . Visualization . Computer Simulations

### Strategy Game
#### Feb 2016 - Jan 2017

<img src="assets/strategy_game.jpeg">

Very basic strategy game using Godot engine  

[https://gitlab.com/lordadamson/strategy_game](https://gitlab.com/lordadamson/strategy_game)

#### Skills:
GDScript . Godot

### FCIS Operating Systems Course Improved Development Environment
#### Feb 2016 - Mar 2016

Associated with Ain Shams University

The Operating Systems course in my faculty was being taught on a 10-year-old PCLinuxOS virtual machine, because the build system just didn't support newer toolchains.  

So I hacked the system and made it possible to develop the system on modern OS without a virtual machine. The TAs graciously included my name in a thank you note, that is included to this day in the course material.  

[https://gitlab.com/lordadamson/FOS-improved-development-environment](https://gitlab.com/lordadamson/FOS-improved-development-environment)

#### Skills:
Problem Solving

### Om 3ala2
#### Jan 2016 - Feb 2016

<iframe width="560" height="315" src="https://www.youtube.com/embed/rKjDyRdghGk?si=V1s6DAikz-D6xpTn" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

It's a game about a little boy who literally has a little chick head. When he got home he stepped on the just-cleaned rug with his dirty shoes. Now the rug is dirty and he's in so much trouble. His mother -who is a woman with a chicken head- now keeps chasing him around the roofs of buildings trying to catch him while he collects coins to buy eggs. Controls: just use the arrow keys space to jump c to crouch Don't forget to collects the coins and have fun :))  

[https://gitlab.com/lordadamson/7dayGame](https://gitlab.com/lordadamson/7dayGame)

#### Skills:
Computer Graphics . Godot . GDScript . 3D Graphics

### CCCSync
#### Sep 2015 - Oct 2015

An alternative to Dropbox that uses a git and cronjobs to synchronize files. Zeneti is used for the UI.  

[https://gitlab.com/lordadamson/CCCSync](https://gitlab.com/lordadamson/CCCSync)

#### Skills:
Bash

### latexCalculator
#### Jun 2015 - Aug 2015

<img src="assets/latex_calculator.jpeg">

This is a calculator that formats mathematical expressions in the way you would normally write them down with pen and paper.  
So instead of having to type lame stuff like:  
`((2+33)/114)\*sqrt(25)`
You would elegantly type it in and it would look like the attached screenshot  

[https://gitlab.com/lordadamson/latexCalculator](https://gitlab.com/lordadamson/latexCalculator)

#### Skills:
Cascading Style Sheets (CSS) . Parsing . JavaScript . HTML

### Open World Game
#### Dec 2013 - Jul 2015

<img src="assets/open_world_game.jpeg">

A game with a boy walking around in an open world. still needs a story and a purpose.  

Implemented in Java JMonkeyEngine  

[https://gitlab.com/lordadamson/OpenWorldGame](https://gitlab.com/lordadamson/OpenWorldGame)

#### Skills:
Java . 3D Graphics