<div class="topnav">
    <a href="/">
        <h1>Projects</h1>
    </a>
    <a href="experience.html" class="active">
        <h1>Experience</h1>
    </a>
    <a href="skills.html">
        <h1>Skills</h1>
    </a>
</div>

# Adham Zahran
adhamzahranfms@gmail.com

A software engineer. A jack of all trades, master of none.

In this website, I will tell you all about my not-so-amazing skills.

## Experience

- ### Luxoft
    - Senior Software Engineer (Mar 2023 - Present)
- ### RDI
    - Technical Lead / Project Manager (Jun 2020 - Mar 2023)
    - C++ Software Engineer (Apr 2017 - Jun 2020)

## Education

- ### Ain Shams University
    - Bachelor's degree, Computer Science

## Volunteering

- ### [OSC - Open Source Community](https://oscgeeks.org/)
    - Co-Founder and President (Jul 2013 - Aug 2015)
        - The community was founded to make people aware of the open source movement, the Linux operating systems, and the different open source software. We also held workshops to learn things like Blender, Linux system administration, Laravel... etc.