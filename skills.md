<div class="topnav">
    <a href="/">
        <h1>Projects</h1>
    </a>
    <a href="experience.html">
        <h1>Experience</h1>
    </a>
    <a href="skills.html" class="active">
        <h1>Skills</h1>
    </a>
</div>

# Adham Zahran
adhamzahranfms@gmail.com

A software engineer. A jack of all trades, master of none.

In this website, I will tell you all about my not-so-amazing skills.

## Skills

### C++ / C++11 / C++14 / C++17
- Salem
- Sotoor, Kateb, Natiq and Tashkeel
- Hand-drawn Animation Autocomplete
- Gollash
- Journal QML
- Digit Recognition
- Journal
- std::tuple and tie() implementation
- Notix
- Heat Flow Simulator
### C
- Ugly UI
- Bath Tracer
- Sneaky Malloc
- The Internet Canvas
### Project Management
- Salem
### Technical Leadership
- Salem
- Sotoor, Kateb, Natiq and Tashkeel
### Databases
- Salem
- Habit Tracker
- php framework
- groupsNmessages
### Multithreading
- Sotoor, Kateb, Natiq and Tashkeel
### Inter-process Communication
- Sotoor, Kateb, Natiq and Tashkeel
### Interop
- Sotoor, Kateb, Natiq and Tashkeel
### Memory Management
- Sotoor, Kateb, Natiq and Tashkeel
### Server Programming
- Sotoor, Kateb, Natiq and Tashkeel
### Network Programming
- Sotoor, Kateb, Natiq and Tashkeel
### Cross-Compiling
- Sotoor, Kateb, Natiq and Tashkeel
### CMake
- Sotoor, Kateb, Natiq and Tashkeel
- Hand-drawn Animation Autocomplete
### Unit Testing
- Sotoor, Kateb, Natiq and Tashkeel
### Continuous Integration and Continuous Delivery (CI/CD)
- Sotoor, Kateb, Natiq and Tashkeel
### Parsing
- Sotoor, Kateb, Natiq and Tashkeel
- Ugly UI
- latexCalculator
### Architectural Design
- Sotoor, Kateb, Natiq and Tashkeel
### Computer Graphics
- Hand-drawn Animation Autocomplete
- Bath Tracer
- Terrain Generation
### Parsing
- Ugly UI
### QML
- Ugly UI
### Lexical Analysis
- Ugly UI
### Inter-process Communication
- Gollash
### Qt
- Gollash
- Journal QML
- Journal
- Notix
- Heat Flow Simulator
### Qt Creator
- Gollash
- Journal QML
- Journal
- Notix
- Heat Flow Simulator
### IPC
- Gollash
### Unix Pipeline
- Gollash
### QML
- Gollash
- Journal QML
- Notix
### Cascading Style Sheets (CSS)
- Bile Manager
- Habit Tracker
- latexCalculator
### Web Servers
- Bile Manager
### JavaScript
- Bile Manager
- Habit Tracker
- The Internet Canvas
- latexCalculator
### HTML
- Bile Manager
- Habit Tracker
- The Internet Canvas
- latexCalculator
### HTML5 Canvas
- Bile Manager
- The Internet Canvas
### Graphical User Interface (GUI)
- Bile Manager
### Go (Programming Language)
- Bile Manager
### SQLite
- Habit Tracker
### Scalable Vector Graphics (SVG)
- Habit Tracker
### PHP
- Habit Tracker
- php framework
- groupsNmessages
### Laravel
- groupsNmessages
- Ketabuk
### Ray Tracing
- Bath Tracer
### Profiler
- Sneaky Malloc
### ffmpeg
- Sneaky Malloc
### Visualization
- Sneaky Malloc
- Heat Flow Simulator
### C#
- Terrain Generation
### Realtime Programming
- The Internet Canvas
### WebSocket
- The Internet Canvas
### Remote Procedure Call (RPC)
- vworld
### Godot
- vworld
- Strategy Game
- Om 3ala2
### GDScript
- vworld
- Strategy Game
- Om 3ala2
### Neural Networks
- Digit Recognition
### Object-Oriented Programming (OOP)
- Journal
### Template Metaprogramming
- std::tuple and tie() implementation
### Bash
- QuickCD
- CCCSync
### Model-View-Controller (MVC)
- php framework
- groupsNmessages
### composer
- php framework
### MySQL
- Salem
- php framework
### MariaDB
- php framework
### Java
- Al Ma7faza - Allowance Calculator
- Open World Game
### Android
- Al Ma7faza - Allowance Calculator
### Computer Simulations
- Heat Flow Simulator
### 3D Graphics
- Om 3ala2
- Open World Game
### Blender
- Om 3ala2
- Open World Game