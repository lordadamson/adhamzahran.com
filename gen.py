import sys
import markdown

def read_file(path):
    content = None
    with open(path, 'r') as f:
        content = f.read()

    assert content
    return content

input_file = sys.argv[1]

template = read_file('template.html')
md = read_file(f'{input_file}.md')
html = markdown.markdown(md)

output = template.replace('{}', html)

with open(f'{input_file}.html', 'w') as f:
    f.write(output)