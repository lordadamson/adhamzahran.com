#!/usr/bin/env bash

set -xe

git commit || echo "Nothing to commit"
git push

ssh root@adhamzahran.com 'cd /var/www/adhamzahran.com/ && git pull'